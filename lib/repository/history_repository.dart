import 'package:http/http.dart' as http;
import 'dart:convert';
import '../models/history.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<History> fetchHistory() async {
  final prefs = await SharedPreferences.getInstance();

  const String url =
      'http://103.23.198.12/eazy_parking_admin/web/index.php?r=api/history-parkir';

  final response = await http.post(
    Uri.parse(url),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, dynamic>{
      "user_id": prefs.getInt('user_id'),
      "page": "1",
      "recordPerPage": "99999"
    }),
  );

  var data = jsonDecode(response.body);
  if (data['status']) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return History.fromJson(data['data']);
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load History');
  }
}
