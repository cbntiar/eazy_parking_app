import 'package:flutter/material.dart';

import '/pages/components/navigation.dart';
import '/pages/login_page.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashPage(),
    );
  }
}

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  bool isLoading = false;
  bool? isLogin = false;
  String message = "";
  TextEditingController npmController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  void initState() {
    // TODO: implement initState
    initialScreen().whenComplete(() => {
          Navigator.pushReplacement<void, void>(
            context,
            MaterialPageRoute<void>(
              builder: (BuildContext context) =>
                  isLogin == true ? const Navigation() : LoginPage(),
            ),
          )
        });

    super.initState();
  }

  Future initialScreen() async {
    final prefs = await SharedPreferences.getInstance();
    var Login = prefs.getBool('login');
    print(isLogin);

    setState(() {
      isLogin = Login;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: FlutterLogo(size: MediaQuery.of(context).size.height));
  }
}
