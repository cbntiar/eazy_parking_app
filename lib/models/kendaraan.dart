class Kendaraan {
  final int totalData;
  final String page;
  final String recordPerPage;
  final List records;

  const Kendaraan({
    required this.totalData,
    required this.page,
    required this.recordPerPage,
    required this.records,
  });

  factory Kendaraan.fromJson(Map<String, dynamic> json) {
    return Kendaraan(
      totalData: json['totalData'],
      page: json['page'],
      recordPerPage: json['recordPerPage'],
      records: json['records'],
    );
  }
}
