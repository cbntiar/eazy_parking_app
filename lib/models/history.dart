class History {
  final int totalData;
  final String page;
  final String recordPerPage;
  final List records;

  const History({
    required this.totalData,
    required this.page,
    required this.recordPerPage,
    required this.records,
  });

  factory History.fromJson(Map<String, dynamic> json) {
    return History(
      totalData: json['totalData'],
      page: json['page'],
      recordPerPage: json['recordPerPage'],
      records: json['records'],
    );
  }
}
