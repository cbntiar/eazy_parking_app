import 'package:eazy_parking/pages/components/navigation.dart';
import 'package:flutter/material.dart';

import 'dart:developer';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:http/http.dart';
import 'dart:convert';
import '../models/kendaraan.dart';
import '../repository/kendaraan_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key, required this.title});

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Future<Kendaraan>? futureKendaraan;

  String? npm = "";
  String? nama = "";
  String? jurusan = "";

  @override
  Future setUser() async {
    final prefs = await SharedPreferences.getInstance();

    setState(() {
      npm = prefs.getString('username');
      nama = prefs.getString('nama');
      jurusan = prefs.getString('jurusan');
    });
  }

  void initState() {
    setUser();
    super.initState();
    futureKendaraan = fetchKendaraan();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          // children: <Widget>[cardKendaraan(), cardTambahKendaraan()],
          children: [
            FutureBuilder<Kendaraan>(
              future: futureKendaraan,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return SizedBox(
                    height: 500,
                    child: ListView.builder(
                        itemCount: snapshot.data?.records.length,
                        itemBuilder: (context, index) {
                          final kendaraan = snapshot.data?.records[index];
                          return cardKendaraan(kendaraan);
                        }),
                  );
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                }

                return const CircularProgressIndicator();
              },
            ),
            cardTambahKendaraan()
          ],
        ),
      ),
    );
  }

  Container cardKendaraan(kendaraan) {
    var nomor_kendaraan = kendaraan['nomor_kendaraan'];
    var kendaraan_id = kendaraan['id'];
    var status = kendaraan['status'];
    var checkout_id = kendaraan['checkout_id'];

    return Container(
      margin: const EdgeInsets.only(left: 40.0, right: 40.0, top: 20),
      child: Card(
        child: InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => QRViewExample(
                nomorKendaraan: nomor_kendaraan,
                id: kendaraan_id,
                status: status,
                checkoutId: checkout_id,
              ),
            ));
          },
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(10),
                child: ListTile(
                  leading: Icon(Icons.motorcycle, size: 35),
                  title: Text(
                    kendaraan['nomor_kendaraan'],
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w900),
                  ),
                  subtitle: Text('Tap untuk melakukan ${status}'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Container cardTambahKendaraan() {
    return Container(
      margin: const EdgeInsets.only(left: 40.0, right: 40.0, top: 20),
      child: Card(
        color: Colors.blue,
        child: InkWell(
          onTap: () {
            // setState(() {
            //   _futureKendaraan = createKendaraan(_controller.text);
            // });
          },
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: const <Widget>[
              Padding(
                  padding: EdgeInsets.all(10),
                  child: Center(
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                      size: 35,
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }

  final TextEditingController _controller = TextEditingController();

  AppBar customAppBar() {
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Colors.blue,
      centerTitle: true,
      title: Text(widget.title),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(30),
        ),
      ),
      // actions: [
      //   InkWell(
      //     onTap: () {},
      //     child: const Padding(
      //       padding: EdgeInsets.all(8.0),
      //       child: Icon(
      //         Icons.notifications,
      //         size: 20,
      //       ),
      //     ),
      //   )
      // ],
      bottom: PreferredSize(
        preferredSize: const Size.fromHeight(110.0),
        child: Container(
            padding: const EdgeInsets.only(left: 30, bottom: 20),
            child: Row(
              children: [
                Stack(
                  children: const [
                    CircleAvatar(
                      radius: 32,
                      backgroundColor: Colors.white,
                      child: Icon(Icons.person_outline_rounded),
                    ),
                  ],
                ),
                Container(
                  margin: const EdgeInsets.only(left: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        nama!,
                        style: const TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.w700,
                            color: Colors.white),
                      ),
                      Text(
                        npm!,
                        style: const TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                      ),
                      Text(
                        "Fakultas - ${jurusan!}",
                        style: const TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                            color: Colors.white),
                      )
                    ],
                  ),
                )
              ],
            )),
      ),
    );
  }
}

class QRViewExample extends StatefulWidget {
  const QRViewExample({
    Key? key,
    required this.nomorKendaraan,
    required this.id,
    required this.status,
    required this.checkoutId,
  }) : super(key: key);

  final String nomorKendaraan;
  final String status;
  final int? checkoutId;
  final int id;

  @override
  State<StatefulWidget> createState() => _QRViewExampleState();
}

class _QRViewExampleState extends State<QRViewExample> {
  Barcode? result;
  bool showSuccessScreen = false;
  bool sendingData = false;
  QRViewController? controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  void parkir() async {
    try {
      DateTime datetime = DateTime.now();
      String dateStr = datetime.toString();

      Map<String, dynamic> requestPayload = {
        "type": widget.status,
        "kendaraan_id": widget.id,
        "time": dateStr
      };

      if (widget.status == 'checkout') {
        requestPayload = {
          "id": widget.checkoutId,
          "type": widget.status,
          "kendaraan_id": widget.id,
          "time": dateStr
        };
      }

      setState(() {
        sendingData = true;
      });

      final response = await post(
        Uri.parse(
            "http://103.23.198.12/eazy_parking_admin/web/index.php?r=api/set-parkir"),
        body: jsonEncode(requestPayload),
        headers: {'Content-Type': 'application/json'},
      );

      var data = jsonDecode(response.body);

      if (data['status']) {
        setState(() {
          showSuccessScreen = true;
          sendingData = false;
        });
        // Navigator.pushReplacement<void, void>(
        //   context,
        //   MaterialPageRoute<void>(
        //       builder: (BuildContext context) => HomePage(title: "Checkin")),
        // );
      }
    } catch (e) {}
  }

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    }
    controller!.resumeCamera();
  }

  AppBar customAppBar() {
    return AppBar(
      backgroundColor: Colors.blue,
      centerTitle: true,
      title: Text("Check In"),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(30),
        ),
      ),
      actions: [
        InkWell(
          onTap: () {},
          child: const Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(
              Icons.notifications,
              size: 20,
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return qrScanScreen(context);
  }

  Scaffold qrScanScreen(BuildContext context) {
    return Scaffold(
        appBar: customAppBar(),
        body:
            // Column(
            //   children: <Widget>[
            //     // Expanded(flex: 4, child: _buildQrView(context)),
            //     Expanded(flex: 4, child: ),
            //   ],
            // ),
            Container(
          // margin: EdgeInsets.only(top: 20, left: 40, right: 40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Card(
                  child: Center(
                child: ListTile(
                  // leading: Icon(Icons.motorcycle, size: 35),
                  title: Text(
                    widget.nomorKendaraan,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w900),
                  ),
                  subtitle: Center(
                    child: Text(
                      widget.status.toUpperCase(),
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                    ),
                  ),
                ),
              )),
              // Expanded(child: _buildQrView(context))
              if (showSuccessScreen)
                // Text('Barcode Type: ${describeEnum(result!.format)}   Data: ${result!.code}')
                Container(
                    margin: EdgeInsets.all(40),
                    child: Center(
                      child: Column(
                        children: [
                          const CircleAvatar(
                            radius: 32,
                            backgroundColor: Colors.white,
                            child: Icon(
                              Icons.check_circle_outline,
                              color: Colors.green,
                            ),
                          ),
                          const SizedBox(height: 24),
                          Text('${widget.status.toUpperCase()} berhasil!'),
                          const SizedBox(height: 24),
                          ElevatedButton(
                            onPressed: () {
                              // Navigator.pushReplacement<void, void>(
                              //   context,
                              //   MaterialPageRoute<void>(
                              //       builder: (BuildContext context) =>
                              //           const Navigation()),
                              // );
                              // Navigator.push(
                              //     context,
                              //     MaterialPageRoute(
                              //         builder: (_) => Navigation()));
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Navigation()),
                                  (route) => false);
                            },
                            child: const Text('OK'),
                          ),
                        ],
                      ),
                    ))
              else
                Column(
                  children: [
                    (() {
                      if (!sendingData) {
                        return Container(
                          width: 400,
                          height: 400,
                          child: _buildQrView(context),
                        );
                      }

                      return const CircularProgressIndicator();
                    })(),
                    Container(
                      margin: const EdgeInsets.all(8),
                      child: ElevatedButton(
                          onPressed: () async {
                            await controller?.toggleFlash();
                            setState(() {});
                          },
                          child: FutureBuilder(
                            future: controller?.getFlashStatus(),
                            builder: (context, snapshot) {
                              return Text(snapshot.data == true
                                  ? 'Matikan Flash'
                                  : 'Nyalakan Flash');
                            },
                          )),
                    ),
                  ],
                ),
            ],
          ),
        ));
  }

  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 250.0
        : 400.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      // overlayMargin: EdgeInsets.all(50),
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
          borderColor: Colors.red,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: scanArea),
      onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
    controller.scannedDataStream.listen((scanData) {
      if (scanData.code == 'EAZY-PARKING-WIDYATAMA') {
        parkir();
        setState(() {
          result = scanData;
        });
      }
    });
  }

  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    log('${DateTime.now().toIso8601String()}_onPermissionSet $p');
    if (!p) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('no Permission')),
      );
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
