import 'package:flutter/material.dart';

import '/pages/components/navigation.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isLoading = false;
  bool? isLogin = false;
  String message = "";
  TextEditingController npmController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  // Navigation()

  void login(String npm, password) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    try {
      setState(() {
        isLoading = true;
        message = "";
      });

      Map<String, dynamic> requestPayload = {
        "username": npm,
        "password": password,
      };

      final response = await post(
        Uri.parse(
            "http://103.23.198.12/eazy_parking_admin/web/index.php?r=api/login"),
        body: jsonEncode(requestPayload),
        headers: {'Content-Type': 'application/json'},
      );

      var data = jsonDecode(response.body);

      if (data['status']) {
        await prefs.setInt('user_id', data['data']['id']);
        await prefs.setString('username', data['data']['username']);
        await prefs.setString('nama', data['data']['nama']);
        await prefs.setString('jurusan', data['data']['jurusan']);
        await prefs.setBool('login', true);
        setState(() {
          isLoading = false;
          message = data['message'];
        });
        Navigator.pushReplacement<void, void>(
          context,
          MaterialPageRoute<void>(
              builder: (BuildContext context) => Navigation()),
        );
      } else {
        setState(() {
          isLoading = false;
          message = data['message'];
        });
      }
    } catch (e) {
      setState(() {
        isLoading = false;
        message = e.toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Eazy Parking Login"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60.0),
              child: Center(
                child: Container(
                    width: 200,
                    height: 150,
                    /*decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(50.0)),*/
                    child: Image.asset('assets/images/logo.png')),
              ),
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                controller: npmController,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'NPM',
                    hintText: 'Masukkan NPM anda'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                controller: passwordController,
                obscureText: true,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Masukkan password'),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            !isLoading
                ? Container(
                    height: 50,
                    width: 250,
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(20)),
                    child: ElevatedButton(
                      onPressed: () {
                        login(npmController.text.toString(),
                            passwordController.text.toString());
                        // Navigator.push(
                        //     context, MaterialPageRoute(builder: (_) => Navigation()));
                      },
                      child: Text(
                        'Login',
                        style: TextStyle(color: Colors.white, fontSize: 25),
                      ),
                    ),
                  )
                : Text("Logging In..."),
            const SizedBox(
              height: 20,
            ),
            Text(message)
          ],
        ),
      ),
    );
  }
}
