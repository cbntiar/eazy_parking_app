import 'package:flutter/material.dart';
// import 'package:flutter_expanded_tile/flutter_expanded_tile.dart';
import '../models/history.dart';
import '../repository/history_repository.dart';

class HistoryPage extends StatefulWidget {
  const HistoryPage({super.key, required this.title});

  final String title;

  @override
  State<HistoryPage> createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  Future<History>? futureHistory;

  late bool _customTileExpanded = false;

  @override
  void initState() {
    super.initState();
    futureHistory = fetchHistory();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar(),
      body: SingleChildScrollView(
          child: Container(
        margin: EdgeInsets.only(top: 20),
        child: Column(
          children: <Widget>[
            // for (var date in logList) cardLogGroup(date),
            FutureBuilder<History>(
              future: futureHistory,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return SizedBox(
                    height: MediaQuery.of(context).size.width * 1.7,
                    child: ListView.builder(
                        itemCount: snapshot.data?.records.length,
                        itemBuilder: (context, index) {
                          final date = snapshot.data?.records[index];

                          return cardLogGroup(date);
                        }),
                  );
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                }

                return const CircularProgressIndicator();
              },
            ),
          ],
        ),
      )),
    );
  }

  Container cardLogGroup(date) {
    return Container(
      margin: EdgeInsets.only(left: 40, right: 40),
      child: ExpansionTile(
        title: Text(date['tanggal']),
        // subtitle: const Text('Custom expansion arrow icon'),
        trailing: Icon(
          _customTileExpanded
              ? Icons.arrow_drop_down_circle
              : Icons.arrow_drop_down,
        ),
        children: <Widget>[
          Container(
              // color: Colors.red,
              child: Column(
            children: [
              Container(
                // margin: EdgeInsets.only(top: 40),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    for (var time in date['data'])
                      time['type'] == "checkin"
                          ? cardCheckIn(time)
                          : cardCheckOut(time),
                  ],
                ),
              )
            ],
          ))
        ],
        onExpansionChanged: (bool expanded) {
          setState(() {
            _customTileExpanded = expanded;
          });
        },
      ),
    );
  }

  Container cardCheckIn(time) {
    return Container(
      // margin: const EdgeInsets.only(left: 40.0, right: 40.0),
      child: Card(
        color: Colors.green[300],
        child: ListTile(
            leading: Icon(Icons.arrow_circle_right_outlined),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Text(
                      time['nomor_kendaraan'],
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      time['time'],
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                    ),
                  ],
                )
              ],
            )),
      ),
    );
  }

  Container cardCheckOut(time) {
    return Container(
      // margin: const EdgeInsets.only(left: 40.0, right: 40.0),
      child: Card(
        color: Colors.red[300],
        child: ListTile(
            leading: Icon(Icons.arrow_circle_left_outlined),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Text(
                      time['nomor_kendaraan'],
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      time['time'],
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                    ),
                  ],
                )
              ],
            )),
      ),
    );
  }

  AppBar customAppBar() {
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Colors.blue,
      centerTitle: true,
      title: Text(widget.title),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(30),
        ),
      ),
      // actions: [
      //   InkWell(
      //     onTap: () {},
      //     child: const Padding(
      //       padding: EdgeInsets.all(8.0),
      //       child: Icon(
      //         Icons.notifications,
      //         size: 20,
      //       ),
      //     ),
      //   )
      // ],
    );
  }
}
