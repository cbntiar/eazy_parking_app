import 'package:eazy_parking/pages/login_page.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_expanded_tile/flutter_expanded_tile.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key, required this.title});

  final String title;

  @override
  State<ProfilePage> createState() => _HistoryPageState();
}

class _HistoryPageState extends State<ProfilePage> {
  late bool _customTileExpanded = false;
  String? npm = "";
  String? nama = "";
  String? jurusan = "";

  Future setUser() async {
    final prefs = await SharedPreferences.getInstance();

    setState(() {
      npm = prefs.getString('username')!;
      nama = prefs.getString('nama')!;
      jurusan = prefs.getString('jurusan')!;
    });
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();

    Navigator.pushReplacement<void, void>(
      context,
      MaterialPageRoute<void>(builder: (BuildContext context) => LoginPage()),
    );
  }

  void initState() {
    setUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar(),
      body: SingleChildScrollView(
          child: Container(
              margin: EdgeInsets.all(40),
              child: Center(
                child: Column(
                  children: [
                    const CircleAvatar(
                      radius: 32,
                      backgroundColor: Colors.white,
                      child: Icon(Icons.person_outline_rounded),
                    ),
                    const SizedBox(height: 24),
                    Text(nama!),
                    const SizedBox(height: 10),
                    Text(npm!),
                    const SizedBox(height: 24),
                    ElevatedButton(
                      onPressed: () {
                        logout();
                      },
                      child: const Text('Logout'),
                    ),
                  ],
                ),
              ))),
    );
  }

  AppBar customAppBar() {
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Colors.blue,
      centerTitle: true,
      title: Text(widget.title),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(30),
        ),
      ),
      // actions: [
      //   InkWell(
      //     onTap: () {},
      //     child: const Padding(
      //       padding: EdgeInsets.all(8.0),
      //       child: Icon(
      //         Icons.notifications,
      //         size: 20,
      //       ),
      //     ),
      //   )
      // ],
    );
  }
}
